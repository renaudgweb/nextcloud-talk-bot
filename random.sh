#!/bin/bash

NUMBER=$[ ( $RANDOM % 3 ) + 1 ]

chuck=$(php /path/to/your/file/chuck.php)
iss=$(php /path/to/your/file/iss.php)
weather=$(weather -mv LFPG)

case $NUMBER in
    "1")
        php /path/to/your/file/bot.php efhy4rgh "$chuck"
        ;;
    "2")
        php /path/to/your/file/bot.php efhy4rgh "$iss"
        ;;
    "3")
        php /path/to/your/file/bot.php efhy4rgh "🌚 🌍$weather🌎 🌔"
        ;;
esac
