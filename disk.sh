#!/bin/bash

# Disk space usage
FS="/usr"
THRESHOLD=90
OUTPUT=($(LC_ALL=C df -P ${FS}))
CURRENT=$(echo ${OUTPUT[11]} | sed 's/%//')
if [ "$CURRENT" -gt "$THRESHOLD" ]
then
    php /path/to/your/file/bot.php efhy4rgh "😱 Disque $FS ($CURRENT% utilisé) est presque plein."

fi
