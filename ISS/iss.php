<?php

$data = file_get_contents('http://api.open-notify.org/astros.json');
$parsed_data = json_decode($data);

$iss_number = $parsed_data->{'number'};
echo "🛰 🛸\n";
echo "Il y a actuellement ".$iss_number." astronautes en orbite:\n";
$iss_crew = $parsed_data;
foreach($iss_crew->people as $value) {
  echo $value->name." (".$value->craft.")\n";
}
echo "🚀 🌏\n";
?>
