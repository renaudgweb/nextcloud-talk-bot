<?php

$data = file_get_contents('https://api.chucknorris.io/jokes/random?category=dev');
$parsed_data = json_decode($data);
$chuck_data = $parsed_data->{'value'};

echo '📢 💬 "' . $chuck_data . '" 👏 🤣';

?>
